#!/bin/bash


function Help()
{
    echo "Usage: $(basename $0) [-bi]."
    echo "  -b  build the extension"
    echo "  -i  install the extension"
}

build=""
install=""

while getopts ":bi" option; do
    case $option in
    b)
        build=1;;
    i)
        install=1;;
    *)
        Help
        exit
        ;;
    esac
done


if [[ $build ]]; then
    EXTRA_SOURCES=""
    for SCRIPT in *.js; do
        EXTRA_SOURCES="${EXTRA_SOURCES} --extra-source=${SCRIPT}"
    done
    
    gnome-extensions pack --force $EXTRA_SOURCES
fi

if [[ $install ]]; then
    gnome-extensions install --force *.zip
fi
